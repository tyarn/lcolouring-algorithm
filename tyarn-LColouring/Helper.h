#ifndef INCLUDED_HELPER
#define INCLUDED_HELPER
#include <iostream>
#include "Graph.h"
#include "LinkedList.h"
#include "Tower.h"
#include <fstream>
using namespace std;


//PRE:TowerNet is a defined Graph of *Towers,
//Lvalues is a defined linkedlist of ints
//and inputFile is readable and an ifstream object.
//POST:reads the inputfile and puts towers into graph and Lvalues into Linkedlist.
void readGraphAndLValues(Graph <Tower *> & TowerNet, LinkedList<int> & LValues, ifstream & inputFile);


//PRE:The graph must be of type Tower * and must exist
//POST:deletes all Towers on heap, and makes their pointers null.
void deAllocatePointers(Graph <Tower *> & TowerNet);

//PRE:TowerNet must be of type Tower *, LValues must be of type int, largestColourNeeded is an int
//POST:colors the TowerNet Towers with valid colors satisfying the LValues, and returns the
//smallest value of the largest colour used to color the TowerNet
void colourGraph(Graph <Tower *> & TowerNet, LinkedList <int> & LValues, int & largestColourNeeded);

 //PRE:TowerNet is a graph of pointers to towers,
  //vertexNum is the number of the node in the linked list
  //vertexList that the vertex we are looking at resides in, LValues
  //is a linkedlist of ints representing the separation values of each
  //vertex and colour, numLValues is the number of nodes in LValues
  //and largest ColourNeeded is the largest colour 
  //needed to colour this graph.
  //POST:recursively checks each vertex and tries to colour the graph
  //with the least amount of colours possible while following rules
  //set by the LValues LinkedList.
bool colourGraphHelper(Graph <Tower *> & TowerNet,int vertexNum,LinkedList <int> & LValues, int numLValues, int largestColourNeeded);

//PRE:TowerNet is a reference to a graph, vertexNum is the index of
//the vertex inside of TowerNet that we are looking at, colour is the
//possible colour we are looking at
//POST:returns false if their are conflicts with the colour possibly
//being given to the vertex at TowerNet.getVertexInfo(vertexNum) and
//returns true otherwise
bool checkEdges(Graph <Tower *> & TowerNet,int vertexNum,int colour, int separationValue, int separationIndex,int largestColour);

//PRE:Vertex contains a pointer to a tower that contains a colour TowerNet contains all vertexs
//vertexNum is the number of the vertex we are getting in towernet
//largest colour is the largest colour that has been used, also
//the largest possible separation between two tower colors.
//POST:returns the smallest separation value between a tower and its neighbours.
int getSmallestSepValue(Vertex <Tower *> aVertex, Graph <Tower *> & TowerNet, int vertexNum,int largestColour);


//PRE:TowerNet is a graph of pointers to valid towers, outfile is the file that
//everything will be printed in, largestColourNeeded is an int representing
//the smallest value of the largest colour used in colouring towerNet.
void printGraph(Graph <Tower *> & TowerNet, ofstream & outFile, int largestColourNeeded);

#endif
