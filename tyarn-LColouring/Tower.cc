#include "Tower.h"
#include <iostream>
using namespace std;
//base constructor
//PRE:
//POST:initialzes Tower object with nothing in it
Tower::Tower(){;
  colorAssigned = false;//variable to see if color has been assigned or not
  isChecked = false;//variable to see if this tower has been checked yet
}

//constructor with number in it
//PRE:towerNum is an int that will initialze the private data towerNumber.
//POST:initialzes tower object with its number.
Tower::Tower(int towerNum){
  towerNumber = towerNum;//name of tower
  colorAssigned = false;//variable to see if color has been assigned or not
  isChecked = false;//variable to see if this tower has been checked yet
}




//PRE:Tower satisfies CI;
//POST:makes the private data colorAssigned equal false
void Tower::colorAssignedFalse(){
  colorAssigned = false;
  //ASSERT:this towers color has not been assigned yet
}

//PRE:Tower satisfies the CI;
//POST:returns the value of colorAssigned
bool Tower::getColorAssigned(){
  return colorAssigned;
}

//PRE:the private member data of color must have a value
//POST:returns color
int Tower::getColor(){
  return color;
}



//PRE:the private member data of towerNumber must have a value
//POST:
int Tower::getData(){
  return towerNumber;
}


//PRE:Tower must satisfy the CI, int pColor is an int
//POST:assigns an int the the private data color, also changes assignedColor to true
void Tower::assignColor(int pColor){
  color = pColor;
  colorAssigned = true;
}


//PRE:
//POST:makes the bool checked that is member data equal true
void Tower::checked(){
  isChecked = true;
}

//PRE:
//POST:returns the boolean member data checked.
bool Tower::getChecked(){
  return isChecked;
}

//PRE:pTower is defined
//POST:creates a deep copy of pTower that will be referenced/
Tower & Tower::operator = (const Tower & pTower){
  towerNumber = pTower.towerNumber;
  color = pTower.color;
  colorAssigned = pTower.colorAssigned;
}

//PRE:pTower satisfies the CI
//POST:creats a deep copy of pTower to be used in a function
Tower::Tower(const Tower & pTower){
  towerNumber = pTower.towerNumber;
  color = pTower.color;
  colorAssigned = pTower.colorAssigned;
}
  

//PRE:both towers are defined and satisfy the CI
//POST:returns true if the towers towerNumber are equal, false otherwise;
bool Tower::operator == (const Tower & pTower)const{
  bool returnVal = false;
  if(towerNumber == pTower.towerNumber){
    returnVal = true;//ASSERT the towerNumbers are the same so they are the same tower as no two towers have the same number.
  }
  return returnVal;
}

ostream & operator << (ostream & stream, const Tower & aTower){
  stream << aTower.towerNumber;
  stream << ",";
  stream << aTower.color;
  return (stream);
}

ostream & operator << (ostream & stream, const Tower * aTower){
  stream << aTower->towerNumber;
  stream<<",";
  stream << aTower->color;
  return (stream);
}


//make ostream operator that prints it out if its a tower *.
