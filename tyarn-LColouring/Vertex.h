#ifndef INCLUDED_VERTEX
#define INCLUDED_VERTEX
#include <iostream>
#include "LinkedList.h"
using namespace std;

template <class T>
//CI:Vertex contains T data, and a linkedlist
class Vertex{
 private:
  T data; //data being stored in vertex node, in this project the data is always a tower.
  LinkedList <int> * edgeList;//list of indices of connected Vertices.
  bool isChecked;//needed for depth first search. always initialzed to false.

 public: 
  //constructor
  //PRE:none
  //POST:initialzes Vertex
  Vertex <T> (){
    edgeList = new LinkedList <int>;
    isChecked = false;
  };
  //constructor
  //PRE:
  //POST: initialzes Vertex
  Vertex <T> (T pData){
    edgeList = new LinkedList<int>;
    isChecked =false;
    data = pData;
  };

  //copy constructor
  //PRE:pVertex satisfies the CI
  //POST:creates a deep copy of pVertex to be used in a function
  Vertex(const Vertex <T> & pVertex){
    data = pVertex.data;
    edgeList = pVertex.edgeList;
    isChecked = pVertex.isChecked;
  };

  
  //PRE:data must exist
  //POST:returns the data from the vertex
  T getData() const{
    return data;
  };

  //PRE:vertex satisfies the CI
  //POST:returns the number of nodes in edgeList
  int getNumEdges() const{
    return edgeList->getNumNodes();
  };

  //PRE:vertex satisfies the CI, index is index of Vertex being
  //represented from the LinkedList in graph class
  //POST:adds an edge to the edgeList.
  void addEdge(int vertexIndex){
    edgeList->addToBack(vertexIndex);//add the index of the vertex that
				    //is an edge to the back of the
				    //edgelist.
  };
  
  //PRE:Vertex satisfies CI
  //POST:changes the bool checked that is memeber data to true.
  void checked(){
    isChecked = true;
  };
  //PRE:vertex satisfies the CI
  //POST:returns the member data checked
  bool getChecked() const{
    return isChecked;
  };
  //PRE:vertex satisfies the CI
  //changes the member data checked to false
  void unCheck(){
    isChecked = false;
  };
  //PRE:Vertex satisfies the CI
  //POST:returns a deep copy of the edgelist
  LinkedList <int> * getEdges() const{
    return edgeList;
  };

  /*
  //PRE:vertex satisfies the CI
  //POST:
  void DFS(LinkedList <T> * aList)const{//this can be const because we
					//are not changing anything in
					//this vertex cause it has
					//been checked.
    int numEdges = getNumEdges();
    cerr<<"This is numEdges " <<numEdges<<endl;
    
    LinkedList <Vertex <T> * > unCheckedList = getUnCheckedVertex();
    //ASSERT:we now have a LinkedList of all unchecked edges of this vertex.
    int numUnChecked = unCheckedList.getNumNodes();
    if(numUnChecked == 0){
      //have return HERE????
    }
    else{
      for(int index = 0; index < numUnChecked; index++){
	Vertex <T> * thisVertex = unCheckedList.getNth(index);
	if(!thisVertex->getChecked()){//check that it is unchecked cause
				    //we dont wanna add it to return
				    //list if it is checked, this
				    //seems repretitve but we need it
				    //for this tower network.
	  aList->addToBack(thisVertex->getData());
	  thisVertex->checked();//this vertex has been checked and
				//added to DF list.
	  thisVertex->DFS(aList);
	  
	}
      }
    }
  };
  */
  
  //PRE:pVertex is defined
  //POST:creates a deep copy of pVertex which the implicit variable is a reference  too.
  Vertex<T> & operator = (const Vertex<T> & pVertex){
    if(&pVertex != this){
      data = pVertex.data;//copy over data
      edgeList = pVertex.edgeList;//copy over edgeList
      isChecked = pVertex.isChecked;//copy of boolean
    }
  };

  //destructor
  ~Vertex(){
   delete edgeList;
  };

  
  //PRE:Vertex satisfies the CI
  //POST:prints the data of the Vertex out.
  friend ostream & operator << (ostream & stream, const Vertex <T> & pVertex){
    stream << pVertex.data;
    return (stream);
    
  };

  
};
#endif
