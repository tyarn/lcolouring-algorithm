#ifndef INCLUDED_TOWER
#define INCLUDED_TOWER
#include <iostream>
using namespace std;

//CLASS CI:For the tower to be valid it must contain only a towerNumber.
class Tower{
 private:
  int towerNumber; //number of tower
  int color;//will not be initialzed;
  bool colorAssigned;//will be initialzed to false;
  bool isChecked;//will be initialzed to false
               //is here for depth first search

  
 public:
  //base constructor
  //PRE:
  //POST:initialzes Tower object with nothing in it
  Tower();

  //constructor with number in it
  //PRE:towerNum is an int that will initialze the private data towerNumber.
  //POST:initialzes tower object with its number.
  Tower(int towerNum);

  //gonna need some more memberfunctions im sure


  //PRE:Tower satisfies CI;
  //POST:makes the private data colorAssigned equal false
  void colorAssignedFalse();

  //PRE:Tower satisfies the CI;
  //POST:returns the value of colorAssigned
  bool getColorAssigned();

  
  //PRE:the private member data of color must have a value
  //POST:returns color
  int getColor();

  //PRE:the private member data of towerNumber must have a value
  //POST:returns the object towerNumber
  int getData();

  //PRE:Tower must satisfy the CI, int pColor is an int
  //POST:assigns an int the the private data color, also changes assignedColor to true
  void assignColor(int pColor);

  //PRE:
  //POST:makes the bool checked that is member data equal true
  void checked();

  //PRE:
  //POST:returns the boolean member data checked.
  bool getChecked();

  //PRE:pTower is defined
  //POST:creates a deep copy of pTower that will be referenced/
  Tower & operator = (const Tower & pTower);

  //PRE:pTower satisfies the CI
  //POST:creats a deep copy of pTower to be used in a function
  Tower(const Tower & pTower);

  //PRE:both towers are defined and satisfy the CI
  //POST:returns true if the towers towerNumber are equal, false otherwise;
  bool operator == (const Tower & pTower) const ;


  //PRE:Tower satisfies the CI
  //POST:prints out the data held in tower.
  friend ostream & operator << (ostream & stream, const Tower & aTower);


  //PRE:Tower satisfies the CI, and tower is a pointer.
  //POST:prints out the data held in tower.
  friend ostream & operator <<  (ostream & stream, const Tower * aTower);
  
};
#endif
