#include "Helper.h"
#include <iostream>
#include <fstream>
#include "Graph.h"
#include "LinkedList.h"
#include "Tower.h"

using namespace std;

//PRE:TowerNet is a defined Graph of *Towers,
//Lvalues is a defined linkedlist of ints
//and inputFile is readable and an ifstream object.
//POST:reads the inputfile and puts towers into graph and Lvalues into Linkedlist.
void readGraphAndLValues(Graph <Tower *> & TowerNet, LinkedList <int> & LValues, ifstream & inputFile){
  int numTowers;//initialze
  int numHolder;//initialze
  inputFile >> numTowers;//get first value from inputFile which
                         //is the number of towers
  for(int tIndex = 0; tIndex < numTowers; tIndex++){

    TowerNet.addVertex(new Tower(tIndex));
    //ASSERT:we have added a vertex
    inputFile >> numHolder;
    for(int eIndex = 0; eIndex < tIndex; eIndex++){
      
      if(numHolder == 1){
	Tower * Tower1 = TowerNet.getVertexInfo(tIndex);//get Towers from this vertex
	
	Tower * Tower2 = TowerNet.getVertexInfo(eIndex);//get Towers from this vertex
	TowerNet.addEdge(Tower1,Tower2);//adds edge in tower1 and tower2
      }
      inputFile >> numHolder; //get rid of ending 0 on line so next
			      //num you get will be first num on new
			      //line.
      
    }
    
    
  }
  //ASSERT:all but last line has been done, graph is made.
  inputFile >> numHolder;
  while(numHolder != 0){
    
    LValues.addToBack(numHolder);
    
    inputFile >> numHolder;
    
  }//ASSERT:We have hit the last 0 in the file so we are done.
  
}



  


//PRE:The graph must be of type Tower * and must exist
//POST:deletes all Towers on heap.
void deAllocatePointers(Graph <Tower *> & TowerNet){
  int numVertexs = TowerNet.getNumVertices();//get the number of
					   //vertices
  Tower * towerHolder;
  for(int index = 0; index < numVertexs; index++){
    towerHolder = TowerNet.getVertexInfo(index);//Iterate through each
					 //vertice and get tower from
					 //it.
    delete towerHolder;
    //ASSERT:towerHolder has been deleted
  }
  //ASSERT:all towers have been deallocated.
}






//PRE:TowerNet must be of type Tower *, LValues must be of type int, largestColourNeeded is an int
//POST:colors the TowerNet Towers with valid colors satisfying the LValues, and returns the
//smallest value of the largest colour used to color the TowerNet
void colourGraph(Graph <Tower *> & TowerNet, LinkedList <int> & LValues, int & largestColourNeeded){
  //colourgraph holder- dont forget when you use getVertices or
  //getNeighbors or getVerticesDepthfirst you need to delete them when you done using cause they
  //dynamically created.
  if(TowerNet.getNumVertices() != 0){
    if(TowerNet.getNumVertices() == 1){
      //special case
      largestColourNeeded = 0;
      Vertex <Tower *> aVertex = TowerNet.getVertex(0);
      Tower * aTower = aVertex.getData();
      aTower->assignColor(0);
    }
    else{
      int numLValues = LValues.getNumNodes();
      largestColourNeeded = 1;//cause we use forloops index to represent
      //colours and coulour things.
      int start = 0;
      bool successful = colourGraphHelper(TowerNet,start,LValues,numLValues,largestColourNeeded);
      while(!successful){
	//ASSERT:the attempt to colour this graph with current largestColourNeeded
	//was unsuccessful
	largestColourNeeded++;//ASSERT:we are adding one colour 
	successful = colourGraphHelper(TowerNet,start,LValues,numLValues,largestColourNeeded);
	
      }
      //ASSERT:graph has been coloured with least amount of colours needed.
    }
  }
}


  //PRE:TowerNet is a graph of pointers to towers,
  //vertexNum is the number of the node in the linked list
  //vertexList that the vertex we are looking at resides in, LValues
  //is a linkedlist of ints representing the separation values of each
  //vertex and colour, numLValues is the number of nodes in LValues
  //and largest ColourNeeded is the largest colour 
  //needed to colour this graph.
  //POST:recursively checks each vertex and tries to colour the graph
  //with the least amount of colours possible while following rules
 //set by the LValues LinkedList.
bool colourGraphHelper(Graph <Tower *> & TowerNet,int vertexNum,LinkedList <int> & LValues, int numLValues, int largestColourNeeded){
  bool returnVal = false;
  //base case
  if(vertexNum == TowerNet.getNumVertices()){
    //ASSERT:we are one node past the last node so end here
    //end recursion
    returnVal = true;
  }
  else{
    Vertex <Tower *> aVertex = TowerNet.getVertex(vertexNum);
    if(TowerNet.getNumEdges(vertexNum) == 0){
      //special Case
      //if the vertex has no edges then we can colour the tower with
      //the smallest colour possilbe 0.
      Tower * aTower = TowerNet.getVertexInfo(vertexNum);
      aTower->assignColor(0);//assign colour to be 0, move onto next vertex
      returnVal = colourGraphHelper(TowerNet,vertexNum + 1, LValues, numLValues, largestColourNeeded);
    }
    else{
      //try colours 0-largestColourNeeded for each vertex here.
      int colourIndex = 0;
      while((!returnVal) && (colourIndex < largestColourNeeded)){
	int LValueIndex = 0;
	bool checkEdgesBool= true;
	while((checkEdgesBool) && (LValueIndex < numLValues)){
	  checkEdgesBool = checkEdges(TowerNet,vertexNum,colourIndex,LValues.getNth(LValueIndex),LValueIndex,largestColourNeeded);
	  LValueIndex++;
	  //if its ever false loop breaks, so must be true throughtout whole thing
	  //to stay true and go into next if
	}
	if(checkEdgesBool){//is this colour usable?
	  //ASSERT:there were no conflicts between all separation values
	  // given and the colour given with the edges of vertex.
	  Tower * aTower = TowerNet.getVertexInfo(vertexNum);
	  aTower->assignColor(colourIndex);
	  returnVal = colourGraphHelper(TowerNet,vertexNum + 1,LValues,numLValues,largestColourNeeded);
	  if(!returnVal){
	    //ASSERT:returnVal came out of recursion with false
	    //so we must deassign color to move and move onto next
	    aTower->colorAssignedFalse();
	  }
	}
	colourIndex ++;//try next colour.
	//ASSERT:we have iterated colour 
      }
    }
  }
  return returnVal;
}

  
//PRE:TowerNet is a reference to a graph, vertexNum is the index of
//the vertex inside of TowerNet that we are looking at, colour is the
//possible colour we are looking at, separation value is the actual
//value we want the colour of the vertex to be separated by,
//separationDepth is the how far away each vertex is from the original.
//POST:returns false if their are conflicts with the colour possibly
//being given to the vertex at TowerNet.getVertexInfo(vertexNum) and
//returns true otherwise
bool checkEdges(Graph <Tower *> & TowerNet,int vertexNum,int colour, int separationValue, int separationDepth, int largestColour){
  LinkedList <Vertex <Tower *> > edges;//initialzes for use
  TowerNet.getEdges(vertexNum,separationDepth,edges);
  //ASSERT:we have a linkedlist of vertices representing the edges at
  //depth separationDepth of the vertex held at vertexNum in TowerNet.
  bool returnVal = true; //initialze returnVal
  int numEdges = edges.getNumNodes();
  int index = 0;
  while((returnVal) && (index < numEdges)){
    Vertex <Tower *> aVertex = edges.getNth(index);
    Tower * aTower = aVertex.getData();
    index++;//iterate index so next loop you use next edge.
    if(aTower->getColorAssigned()){//if colour is not assigned we dont care about vertex
      //ASSERT:this tower has been assigned a colour
      int towerCol = aTower->getColor();//ASSERT:the colour from this tower is held towerCol           
      int larger;//initialze
      int smaller;//initialze
      if(towerCol > colour){
	larger = towerCol;
	smaller = colour;
      }
      else{
	larger = colour;
	smaller = towerCol;
      }//ASSERT: we have found the largest of the two values and make it
      //equal the integer largest, same with smallest
      int sepVal = larger - smaller;
      //largestColour acts as a sort of zero.
      //so if largerCircValue reaches largestColour it has reached 0
      int smallerCircValue = largestColour + smaller;
      int largerCircValue = larger + separationValue;
      //if smallercircValue is equal to or greater than largerCircValue
      //it means that it is separated by separationValue or more colours
      //so it is true else it is false.
      if((sepVal < separationValue) || (smallerCircValue < largerCircValue)){
	returnVal = false;
      }//ASSERT:if returnVal becomes false it will exit the loop and
      //return false
      }
    }
  TowerNet.unCheckVertices();
  //unCheck the vertices in TowerNet that got checked when calling
  //getEdges so you can perform getEdges next time checkEdges is called.
  return returnVal;
}



//PRE:Vertex contains a pointer to a tower that contains a colour TowerNet contains all vertexs
//vertexNum is the number of the vertex we are getting in towernet
//largest colour is the largest colour that has been used, also
//the largest possible separation between two tower colors.
//POST:returns the smallest separation value between a tower and its neighbours.
int getSmallestSepValue(Vertex <Tower *> aVertex, Graph <Tower *> & TowerNet, int vertexNum,int largestColour){
  int returnVal =largestColour - 1;
  //ASSERT:this is largest possible separation value between two towers.
  Tower * thisTower = aVertex.getData();
  //ASSERT:this is vertex we are getting smallestsepvalue for
  LinkedList <Vertex <Tower *> > edges;//initialzes for use
  TowerNet.getEdges(vertexNum,0,edges);
  //ASSERT:these are the edges at depth 0 of our vertex
  int numEdges = edges.getNumNodes();
  //ASSERT:we have the number of edges in thisVertex.
  for(int index = 0; index < numEdges; index++){
    Vertex <Tower *> bVertex = edges.getNth(index);
    Tower * bTower = bVertex.getData();
    //ASSERT:we have the tower containted in edge index.
    if(thisTower->getColor() > bTower->getColor()){
      //ASSERT:thistower color is greater than bTower color
      int newNum = thisTower->getColor() - bTower->getColor();
      if(newNum < returnVal){
	//ASSERT:newNum(separation value) is less than returnValue
	returnVal = newNum;
      }
    }
    else{
      //ASSERT:btower color is greater than thisTowercolor.
      int newNum = bTower->getColor() - thisTower->getColor();
      if(newNum < returnVal){
	//ASSERT:newNum(separation value) is less than returnVal;
	returnVal = newNum;
      } 
    }
  }
  return returnVal;
  
  
}

//PRE:TowerNet is a graph of pointers to valid towers, outfile is the file that
//everything will be printed in, largestColourNeeded is an int representing
//the smallest value of the largest colour used in colouring towerNet.
//POST:
void printGraph(Graph <Tower *> & TowerNet, ofstream & outFile, int largestColourNeeded){
  //change all couts to outfile
  outFile<<"The amount of colours you need is "<<largestColourNeeded;
  outFile<<" or Colours 0 - "<<largestColourNeeded - 1<<endl; 
  int numNodes = TowerNet.getNumVertices();
  
  for(int index = 0; index < numNodes; index++){
    Vertex <Tower *> aVertex = TowerNet.getVertex(index);
    int smallestSepValue = getSmallestSepValue(aVertex,TowerNet,index,largestColourNeeded);
    Tower * aTower = TowerNet.getVertexInfo(index);
    outFile << aTower <<","<<smallestSepValue<<endl;
    //ASSERT:we have printed out data in aTower
    //move onto next one.
  }
  //ASSERT:we are done printing everything from TowerNet.
  
}

