// This class create a graph where the type T is the information, about
// the vertex, stored at each vertex.
#ifndef INCLUDED_GRAPH
#define INCLUDED_GRAPH

#include "Vertex.h"
#include "LinkedList.h"

template <class T>
class Graph {

  // Objects of this class store undirected graphs.
  
  // contains a LinkedList of Vertexs of type T.

 private:

  // MEMBER DATA HERE 
  LinkedList < Vertex <T> > vertexList;//LinkedList of vertexs of type T.

  // The graph class must store the vertices as a linked list. Each of
  // the nodes of this linked list must contain an object of type T,
  // i.e., the information for a vertex, and a linked list (or a
  // pointer to a linked list) that contains the edges that this
  // vertex is a member of. Each of the nodes in the list of edges
  // must contain, as its data, a pointer to the appropriate node in the
  // list of vertices.

 public:
  // PRE:none
  // POST: This graph is an empty graph.
  Graph<T> (){};//dont need anything because linkedlist
              //is all we have for member data
              // and its already made with nothing in it. 

  // PRE: G satisfies the CI
  // POST: This graph is a deep copy of G.
  Graph<T> (const Graph<T> & G){
    vertexList = G.vertexList;
  };

  // PRE: G is defined.
  // POST: RV is (a reference to) a graph that is a deep copy of G. 
  Graph<T> & operator = (const Graph<T> & G){
    vertexList = G.vertexList;
  };
  
  // PRE: data is defined, and there is no vertex in this graph
  //         containing data.
  // POST: This graph contains a vertex containing data.
  //         The new vertex is not a member of any edges.
  void addVertex (const T & data){
    vertexList.addToBack(data);//adds a vertex to the back of the linkedlist
  };

  // PRE: fromData and toData are defined, and this graph contains a
  //        vertex, u, containing fromData as 
  //        information, and a vertex, v, containing toData as
  //        information.
  // POST: An edge is added connecting u and v.
  void addEdge (const T & fromData, const T & toData){
    //initialze Vertex holders.
    Vertex <T> vertex1;//initialze the vertex1
    Vertex <T> vertex2;//initialze the vertex2
    int vertex1Index = vIndexOf(fromData);//find the index of the
					  //vertex that contains the
					  //data fromData.
    vertex1 = vertexList.getNth(vertex1Index);//getvertex
    
    int vertex2Index = vIndexOf(toData);//find the index of the
					//vertex that contains
					//the data toData
    vertex2 = vertexList.getNth(vertex2Index);//get vertex

    vertex1.addEdge(vertex2Index);//addEdge for vertex1 of vertex2
    vertex2.addEdge(vertex1Index);//addEdge for vertex2 of vertex1
    
    
    
  };
  

  // PRE: data is defined.
  // POST: RV = the index of the vertex, if any, containing data.
  //          If no such vertex exists, RV = -1.
  int vIndexOf (const T & data) const{
    int numVertices = getNumVertices();
      //ASSERT:we have the number of vertices in this graph;
      int returnVal;//initialze
    for(int index = 0; index < numVertices; index++){
      if(data == getVertexInfo(index)){
	returnVal = index;
	return returnVal;//ASSERT:correct data has been found.
      }
    }
  };

  //PRE:int index is the index of the vertex that we are looking at,
  //depth is the depth of the search we want to do. returnEdges is the
  //linkedlist that everything will be added too.
  //POST:returns a linkedList of vertexs that are edges of the vertex
  //specified by the index given from a breadth first search
  //perspective where the depth is specified by a parameter.
  void getEdges(int index, int depth,LinkedList <Vertex <T> > & returnEdges){
    Vertex <T> aVertex = vertexList.getNth(index);
    //ASSERT:we have the correct vertex we want.
    int numEdges = aVertex.getNumEdges();
    //ASSERT:we have the number of edges this vertex has.
    LinkedList <int> * edgeList = aVertex.getEdges();
    //ASSERT:we have all indices of edges of our initial vertex.
    //base case
    if(depth == 0){
      //ASSERT:we are at the depth we want.
      for(int index = 0; index < numEdges; index++){
	int numVertex = edgeList->getNth(index);
	Vertex <T> bVertex = vertexList.getNth(numVertex);
	if(!bVertex.getChecked()){
	  //ASSERT:this vertex hasnt been checked so hasnt been
	  //added into list yet
	  bVertex.checked();//bVertex has been checked so it wont be
			    //added to list if it is looked at again
	  returnEdges.addToBack(bVertex);
	  
	}
      }
    }
    else{
      for(int index = 0; index < numEdges; index++){
	int numVertex = edgeList->getNth(index);
	//ASSERT: we get the indice of one of the edges
	Vertex <T> bVertex = vertexList.getNth(numVertex);
	//ASSERT:this is an edge of aVertex.
	bVertex.checked();
	//ASSERT:this vertex is checked
	getEdges(numVertex, depth - 1, returnEdges);
	//ASSERT:going down to next depth.
      }
    }
  };

  //PRE:none
  //POST:to be used after performing getEdges so the vertexs can have
  //their individual edges gotten again
  void unCheckVertices(){
    int numVertices = getNumVertices();
    //ASSERT:we have the correct number of vertices in graph 
    for(int index = 0; index < numVertices; index ++){
      Vertex <T> aVertex = vertexList.getNth(index);
      aVertex.unCheck();
    }//ASSERT:all vertices have been unchecked.
  };

  //PRE:VertexList must have more than 0 nodes, index is the index of
  //the node we want to get
  //POST:returns a vertex object of type T
  Vertex <T> getVertex(int index){
    Vertex <T> returnVertex = vertexList.getNth(index);
    return returnVertex;
  };
  
  //PRE:none
  // POST: RV = the number of vertices in this graph.
  int getNumVertices () const{
    return vertexList.getNumNodes();
  };

  // PRE: vIndex < number of vertices in this graph
  // POST: RV = the vertex information for vertex at vIndex.
  T getVertexInfo (int vIndex) const{
    Vertex <T> thisVertex = vertexList.getNth(vIndex); //this gives you the data of node, which will be a vertex in the way we made the class,
    return thisVertex.getData();//using a vertex function to get the data out of it which is of type T
    
    
  };

  // PRE: vIndex < number of vertices in this graph
  // POST: RV = the number of edges that the vertex at vIndex is a
  //              member of. 
  int getNumEdges (int vIndex) const{
    Vertex <T> thisVertex = vertexList.getNth(vIndex);//ASSERT we have the correct Vertex so we can get stuff out of it.
    return thisVertex.getNumEdges();//retrun the number of edges for that vertex.
  };


  
  // POST: RV = a pointer to a list of vertices in some order (perhaps
  //              the order in which the vertices were created).
  //              This is a dynamically created list
  //              and needs to be deleted after use.
  //            The returned list must be a deep copy of the list of
  //              vertices stored in the graph.
  LinkedList<T> * getVertices () const{//dont need this cause I already have a linkedlist of vertex
    /*
    LinkedList <T> * returnList;
    int numNodes = vertexList.getNumNodes();
    for(int index = 0; index < numNodes; index++){
      Vertex <T> aVertex = vertexList.getNth(index);//This gets the node data which is a vertex<T>
      returnList->addToBack(aVertex.getData());//adds the data which is of type T to the back of list.
    }
    return returnList;//client knows this list is dynamically created so it is their job to delete.
                      //so need to delete in functions that it is used in after it is done being used.
    //THERE IS A SEG FAULT SOMEWHERE IN HERE.    
    */

    //dont need this function cause we are using linkedlist.
  };


  // PRE: This is a connected graph.
  // POST: RV = a pointer to a list of vertices in depth-first order
  //              traversal. 
  //              This is a dynamically created list
  //              and needs to be deleted after use.
  LinkedList<T> * getVerticesDepthFirst (){//no const cause its
					   //changing stuff about
					   //vertexs in linkedlist
    /*
    LinkedList<T> * returnTowerList = new LinkedList<T>();//initialze returnlist
    int numVertices = getNumVertices();//get number of vertices
    for(int index = 0; index < numVertices; index++){
      //check if vertex is checked
      //if it has been checked do not perform DFS on it because
      //if it has been checked it means that it has already
      //had all its edges looked at and so on.
      Vertex <T> aVertex = vertexList.getNth(index);
      if(!aVertex.getChecked()){
	//ASSERT:vertex hasnt been checked.
        DFS(aVertex,returnTowerList);
	//ASSERT:DFS has finished for that Vertex
      }
    }//ASSERT:DepthFirst search is done so go uncheck every vertex

      for(int index2 = 0; index2 < numVertices; index2++){
	Vertex <T> aVertex = vertexList.getNth(index2);
	aVertex.unCheck();//ASSERT:have unchecked this vertex
      }//ASSERT:have unchecked every vertex in linkedlist.
      return returnTowerList; 
    */
  };
      
  
  // PRE: vIndex < number of vertices in this graph
  // POST: RV = a pointer to a list of vertices (in any order) that
  //              contain information for those vertices v such that u-v is an
  //              edge in the graph (where u is the vertex at vIndex).
  //              This is a dynamically created list
  //              and needs to be deleted after use.
  //            The returned list must be a deep copy, containing the
  //              data from the edges, of the list of 
  //              edges stored in the graph.
  LinkedList<T> * getNeighbours (int vIndex) const{
    //create vertex function to return a deep copy of edges LinkedList.
    //Navigate to it using getNth.
    LinkedList <T> * returnList;//initialze return list
    Vertex <T> findVertex = getVertexInfo(vIndex);//ASSERT:have the correct vertex object
    LinkedList <int> edges = findVertex.getEdges();//ASSERT:have
						   //indexs of edges
    int numNodes = edges.getNumNodes();
    for(int index = 0; index < numNodes; index++){
      int numVertex = edges.getNth(index);//ASSERT:we have a number of
					  //a vertex that is an edge
					  //in vertexList
      Vertex <T> aVertex = vertexList.getNth(aVertex);//ASSERT:we have
						      //a vertex
      T addData = aVertex.getData();
      returnList->addToBack(addData);
    }
    //ASSERT:we have added all towers that are neighbors to the vertex
    //at vIndex into returnList.
    return returnList;//client knows this list is dynamically created so it is their job to delete it.  
  };

  // PRE: data is defined, and a vertex, u, in the graph contains data.
  // POST: RV = a pointer to a list of vertices (in any order) that
  //              contain information for those vertices v such that u-v is an
  //              edge in the graph.
  //              This is a dynamically created list
  //              and needs to be deleted after use.
  //            The returned list must be a deep copy of the list of
  //              edges stored in the graph.
  LinkedList<T> * getNeighbours (const T & data) const{
    int indexOf = vIndexOf(data);//ASSERT:have the index of the vertex
				 //that holds T data
    LinkedList<T> * returnList; //initialze returnlist
    returnList = getNeighbours(indexOf);
    //ASSERT:using the function above, we now have all towers that are
    //neighbors to the vertex that holds T Data
    return returnList;
    
  };

  // PRE: vIndex < number of vertices in this graph.
  // POST: The vertex at vIndex is removed from the graph along with
  //          any edges that the vertex is a member of.
  void deleteVertex (int vIndex){
  };

  // PRE: data is defined.
  // POST: The vertex, if any, containing data is removed from the
  //          graph along with any edges that the vertex is a member
  //          of. 
  void deleteVertex (const T & data){
  };

  // PRE: vInindex < number of vertices in this graph.
  //      eIndex < number of edges that the vertex at vIndex is a
  //         member of
  // POST: The edge at eIndex that the vertex at vIndex is a part of // ask shende if this should be switched... or to explain this one.
  //         is removed from the graph.
  void deleteEdge (int vIndex, int eIndex);

  // PRE: fromData and toData are defined.
  // POST: The edge, if any, containing the vertex, if any, containing
  //         fromData and the vertex, if any, containing toData is
  //         removed from the graph.
  void deleteEdge (const T & fromdata, const T & toData);


  // Destructor
  ~Graph<T>(){
  };
};
#endif
